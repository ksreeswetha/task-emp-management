const express = require('express');
const body_parser = require('body-parser');

const pool = require('./db/db');
const controller = require('./controllers/employee');

const port = process.env.PORT || 3000;

const app = express();

// use body parser middleware
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: true}))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// get all employee details
app.get('/', async (req, res) => {
    console.log("get all")
    await controller.getAllEmp(req, res);
});

//get employee details by id
app.get('/:id', async (req, res) => {
    console.log("get by id - " + parseInt(req.params.id));
    await controller.getEmpById(req, res, parseInt(req.params.id));
});

// add a new employee
app.post('/addEmp', async(req, res) => {
    console.log("add emp - " + req.body);
    let emp = {
        "name" : req.body.name,
        "id" : parseInt(req.body.id),
        "active" : req.body.active
    };
    await controller.addEmp(req, res, emp);
});

// update details of existing employee
app.post('/:id', async(req, res) => {
    console.log("update emp by id - " + req.body);
    console.log(req.body)
    let emp = {
        "id": req.params.id,
        "name" : req.body.name,
        "active" : req.body.active
    };
    await controller.updateEmp(req, res, emp);
});

// delete existing employee
app.delete('/:id', async(req, res) => {
    console.log("delete emp by id - " + parseInt(req.params.id));
    await controller.deleteEmp(req, res, parseInt(req.params.id));
});

app.listen(port);