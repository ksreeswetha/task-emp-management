const Pool = require('pg').Pool;
const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
    ssl: true
    // user: 'postgres',
    // host: 'localhost',
    // database: 'Test',
    // password: 'postgres',
    // port: 5432
});

pool.connect();

module.exports = pool;