class Response {

    constructor(statusCode, statusMessage, data) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.data = data;
    }

    success(req, res) {
        res.send({
            status: true,
            statusCode: this.statusCode,
            statusMessage: this.statusMessage,
            data: this.data
        });
    }

    error(req, res) {
        res.send({
            status: false,
            statusCode: this.statusCode,
            statusMessage: this.statusMessage,
            data: this.data
        });
    }
}

module.exports = { Response }