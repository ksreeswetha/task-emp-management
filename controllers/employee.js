const express = require('express');
const pool = require('../db/db');
const responseHandler = require('./responseHandler');

async function getAllEmp(req, res) {
    try{
        await pool.query(`
             SELECT * FROM "employee"
            `).then( result => {
                let response = new responseHandler.Response(200, 'List of all employees', result.rows);
                response.success(req, res);
            }).catch( err => {
                let response = new responseHandler.Response(500, 'Internal server error', err);
                response.error(req, res);
            });
    }
    catch(e){
        let response = new responseHandler.Response(500, 'Internal server error', e);
        response.error(req, res);
    }
}

async function getEmpById(req, res, id) {
    try{
        await pool.query(`
              SELECT * FROM "employee" WHERE id='` + id + `'
            `).then( result => {
                if(result.rowCount > 0) {
                    let response = new responseHandler.Response(200, 'Details of employee with ID ' + id, result.rows);
                    response.success(req, res);
                } else {
                    let response = new responseHandler.Response(204, 'Employee with ID ' + id + ' does not exist', []);
                    response.success(req, res);
                }
            }).catch( err => {
                let response = new responseHandler.Response(500, 'Internal server error', err);
                response.error(req, res);
            });
    }
    catch(e){
        let response = new responseHandler.Response(500, 'Internal server error', e);
        response.error(req, res);
    }
}

async function addEmp(req, res, emp) {
    try{
        await pool.query(
            `INSERT INTO "employee" (
                id, name, active) VALUES (
                '`+ emp.id +`'::bigint, '`+ emp.name +`'::character varying, `+ emp.active +`::boolean)
                    returning id;`
            ).then( result => {
                let response = new responseHandler.Response(201, 'Added new employee with ID ' + emp.id, result.rows);
                response.success(req, res);
            }).catch( err => {
                let response = new responseHandler.Response(500, 'Internal server error', err);
                response.error(req, res);
            });
    }
    catch(e){
        let response = new responseHandler.Response(500, 'Internal server error', e);
        response.error(req, res);
    }
}

async function updateEmp(req, res, emp) {
    try{
        await pool.query(
            `UPDATE "employee" SET
            active = `+ emp.active +`::boolean, name = '`+ emp.name +`'::character varying WHERE
            id = `+ emp.id + `returning *;
        `).then( result => {
                if(result.rowCount > 0) {
                    let response = new responseHandler.Response(200, 'Updated details of employee with ID ' + emp.id, result.rows);
                    response.success(req, res);
                } else {
                    let response = new responseHandler.Response(204, 'Employee with ID ' + emp.id + ' does not exist', result.rows);
                    response.success(req, res);
                }
            }).catch( err => {
                let response = new responseHandler.Response(500, 'Internal server error', err);
                response.error(req, res);
            });
    }
    catch(e){
        let response = new responseHandler.Response(500, 'Internal server error', e);
        response.error(req, res);
    }
}

async function deleteEmp(req, res, id) {
    try{
        await pool.query(
            `DELETE FROM "employee" WHERE id = '`+ id +`' returning id;`
        ).then( result => {
            if (result.rowCount > 0) { 
                let response = new responseHandler.Response(200, 'Successfully deleted employee with ID ' + id, result.rows);
                response.success(req, res);
            } else {
                let response = new responseHandler.Response(204, 'Employee with ID '+ ' does not exist' + id, result.rows);
                response.success(req, res);
            }
        }).catch( err => {
            let response = new responseHandler.Response(500, 'Internal server error', err);
            response.error(req, res);
        });
    }
    catch(e){
        let response = new responseHandler.Response(500, 'Internal server error', e);
        response.error(req, res);
    }
}
module.exports = { getAllEmp, getEmpById, addEmp,updateEmp, deleteEmp, };